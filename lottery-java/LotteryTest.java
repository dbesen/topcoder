import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LotteryTest {

    protected Lottery solution;

    @Before
    public void setUp() {
        solution = new Lottery();
    }

    @Test(timeout = 2000)
    public void testCase0() {
        String[] rules = new String[]{"PICK ANY TWO: 10 2 F F", "PICK TWO IN ORDER: 10 2 T F", "PICK TWO DIFFERENT: 10 2 F T", "PICK TWO LIMITED: 10 2 T T"};

        String[] expected = new String[]{"PICK TWO LIMITED", "PICK TWO IN ORDER", "PICK TWO DIFFERENT", "PICK ANY TWO"};
        String[] actual = solution.sortByOdds(rules);

        Assert.assertArrayEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase1() {
        String[] rules = new String[]{"INDIGO: 93 8 T F", "ORANGE: 29 8 F T", "VIOLET: 76 6 F F", "BLUE: 100 8 T T", "RED: 99 8 T T", "GREEN: 78 6 F T", "YELLOW: 75 6 F F"};

        String[] expected = new String[]{"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"};
        String[] actual = solution.sortByOdds(rules);

        Assert.assertArrayEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase2() {
        String[] rules = new String[]{};

        String[] expected = new String[]{};
        String[] actual = solution.sortByOdds(rules);

        Assert.assertArrayEquals(expected, actual);
    }

    @Test
	public void testThings() {
    	verify("name: 1 1 T T", 1);
    	verify("name: 1 1 T F", 1);
    	verify("name: 1 1 F T", 1);
    	verify("name: 1 1 F F", 1);
    	
    	verify("name: 5 3 T T", 10);
    	verify("name: 5 3 T F", 35);
    	verify("name: 5 3 F T", 60);
    	verify("name: 5 3 F F", 125);

    	verify("name: 3 3 T T", 1);
		verify("name: 3 3 T F", 10);
		verify("name: 3 3 F T", 6);
		verify("name: 3 3 F F", 27);
	}
    
    @Test
	public void testOddsExample0() {
		verify("PICK ANY TWO: 10 2 F F", 100);
		verify("PICK TWO IN ORDER: 10 2 T F", 55);
		verify("PICK TWO DIFFERENT: 10 2 F T", 90);
		verify("PICK TWO LIMITED: 10 2 T T", 45);
	}
    
    @Test
	public void testOddsExample1() {
		String yellow_rule = "YELLOW: 75 6 F F";
		String green_rule = "GREEN: 78 6 F T";
		assertTrue(getOdds(yellow_rule) < getOdds(green_rule));
		
		verify(yellow_rule, 177978515625l);
		verify(green_rule, 184933148400l);
	}

	private void verify(String rule, long expected) {
		long odds = getOdds(rule);
		assertEquals(expected, odds);
	}

	private long getOdds(String rule) {
		LotteryItem item = new LotteryItem(rule);
		long odds = item.getOdds();
		return odds;
	}
	
	@Test
	public void testPermutateWithoutRepitition() {
		verify("name: 16 3 F T", 3360);
		assertEquals(3360, LotteryItem.permutateWithoutRepetition(16, 3));
		
	}

	@Test
	public void testCombinationWithoutRepitition() {
		verify("name: 16 3 T T", 560);
		assertEquals(560, LotteryItem.combinationWithoutRepitition(16, 3));
		
	}
}
