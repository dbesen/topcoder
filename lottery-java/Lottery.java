import java.util.ArrayList;
import java.util.Collections;

public class Lottery {

	public String[] sortByOdds(String[] rules) {
		ArrayList<LotteryItem> list = new ArrayList<LotteryItem>();
		for(String rule : rules) {
			LotteryItem item = new LotteryItem(rule);
			list.add(item);
		}
		
		Collections.sort(list);
		String[] toReturn = new String[rules.length];
		for(int i=0;i<toReturn.length;i++) {
			toReturn[i] = list.get(i).getName();
		}
		return toReturn;
	}
	
}

class LotteryItem implements Comparable<LotteryItem> {
	private String name;
	private int choices;
	private int blanks;
	private boolean sorted;
	private boolean unique;
	
	private long odds;
	
	public String getName() {
		return name;
	}

	public int getChoices() {
		return choices;
	}

	public int getBlanks() {
		return blanks;
	}

	public boolean isSorted() {
		return sorted;
	}

	public boolean isUnique() {
		return unique;
	}
	
	public long getOdds() {
		return odds;
	}
	
	public LotteryItem(String rules) {
		String[] parts = rules.split(":");
		name = parts[0];
		String[] rest = parts[1].split(" ");
		choices = Integer.parseInt(rest[1]);
		blanks = Integer.parseInt(rest[2]);
		sorted = rest[3].equals("T");
		unique = rest[4].equals("T");
		odds = calculateOdds();
	}

	private long calculateOdds() {
		if(!sorted && !unique) {
			return (long) Math.pow(choices, blanks);
		}
		
		else if(!sorted && unique) {
			return permutateWithoutRepetition(choices, blanks);
		}
		
		else if(sorted && unique) {
			return combinationWithoutRepitition(choices, blanks);
		}
		// sorted && !unique
		return combinationWithoutRepitition(choices + blanks - 1, blanks);
	}

	public static long combinationWithoutRepitition(int choices, int blanks) {
		long result = permutateWithoutRepetition(choices, blanks);
		for(int i=1;i<=blanks;i++)
			result /= i;
		return result;
	}

	public static long permutateWithoutRepetition(int choices, int blanks) {
		long result = 1;
		for(int i=(choices - blanks) + 1; i <= choices; i++) {
			result *= i;
		}
		return result;
	}

	public int compareTo(LotteryItem that) {
		if(this.odds == that.odds) {
			return this.name.compareTo(that.name);
		}
		
		return new Long(this.odds).compareTo(that.odds);
	}
}

