import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


public class PenLift {

	// SRM 144 DIV 1 - 1100 points
	// I had no idea how to do this one, so I cheated and looked at the editorial
	public int numTimes(String[] segments, int n) {
		// Combine all overlapping lines
		// Break all lines down so intersections are vertices
		// Break graph down into connected components
		// If n is even, calculate and return the answer
		// Count odd vertices in each connected component
		
		// ^^ that's stupid
		// we don't have to combine overlapping lines, we just have to know for each vertex, does it have a line going n/s/e/w
		// max paths from a vertex is 4!
		// so...
		//
		// For each line endpoint, make a vertex and mark it as having a path for each direction
		// Calculate all intersections
		// For each intersection, mark that vertex as having appropriate paths
		// But what about figuring out how many connected components there are?
		// - If a point has a line going west, we know it's connected to the next point to its west!
		
		// Operations:
		// pointHasDirection(2, 3, north)
		// doesPointHaveDirection(2, 3, north)
		// countNumEdges(2, 3) // max 4
		// getAdjacentPoint(2, 3, north) // this is the hard one
		// pointIsVisited(2, 3)
		// clearVisited() // ?
		
		// ^^ that's stupid, we *do* have to combine overlapping lines, or the directions won't be set properly

		
		LineSegments lineSegments = new LineSegments(segments);
		lineSegments.combineOverlapping();
		
		System.out.println("Line segments after combining (" + lineSegments.getNumSegments() + "):");
		for(LineSegment segment : lineSegments.getSegments()) {
			System.out.println(segment);
		}

		PointList pointList = lineSegments.getPointList();
		
		// Now our point list is built.  We're ready to do the real problem.
		System.out.println("Number of points: " + pointList.size());
		
		Collection<PointList> components = pointList.splitIntoConnectedComponents();
		System.out.println("Num components: " + components.size());

		// If n is odd, we need a lift for each pair of odd vertices after the first
		if(n % 2 != 0) {
			int numLifts = 0;
			System.out.println("n is odd");
			for(PointList list : components) {
				int numOddVertices = list.getNumOddVertices();
				System.out.println("numOddVertices: " + numOddVertices);
				numLifts += Math.max(1, numOddVertices / 2);
				System.out.println("num lifts so far: " + numLifts);
			}
			numLifts -= 1;
			System.out.println("Returning " + numLifts);
			return numLifts;
		} else {
			System.out.println("n is even");
			int numLifts = components.size() - 1;
			System.out.println("Returning " + numLifts);
			return numLifts;
		}
	}


}

class LineSegments {
	private ArrayList<LineSegment> segments = new ArrayList<LineSegment>();
	
	public LineSegments(String[] segmentsArg) {
		for(String segmentStr : segmentsArg) {
			segments.add(new LineSegment(segmentStr));
		}
	}

	public PointList getPointList() {
		PointList pointList = new PointList();
		for(LineSegment segment : segments) {
			if(segment.isVertical()) {
				Point lowerPoint = segment.getPoint1();
				lowerPoint.setHasNorth();
				pointList.addPoint(lowerPoint);
				Point upperPoint = segment.getPoint2();
				upperPoint.setHasSouth();
				pointList.addPoint(upperPoint);
			} else {
				Point leftPoint = segment.getPoint1();
				leftPoint.setHasEast();
				pointList.addPoint(leftPoint);
				Point rightPoint = segment.getPoint2();
				rightPoint.setHasWest();
				pointList.addPoint(rightPoint);
			}
		}
		
		for(int i=0;i<segments.size();i++) {
			for(int j=i+1;j<segments.size();j++) {
				Point p = Point.intersect(segments.get(i), segments.get(j));
				if(p != null)
					pointList.addPoint(p);
			}
		}
		return pointList;
	}

	public void combineOverlapping() {
		System.out.println("Got here");
		System.out.println(this);
		for (int i = 0; i < segments.size(); i++) {
			for (int j = i + 1; j < segments.size(); j++) {
				LineSegment iSegment = segments.get(i); // we have to re-get i since it might be modified during the loop
				LineSegment combined = iSegment.combineWith(segments.get(j));
				if (combined != null) {
					segments.set(i, combined);
					segments.remove(j);
					j--;
					System.out.println("After combine");
					System.out.println(this);
				}
			}
		}
	}

	public int getNumSegments() {
		return segments.size();
	}

	public List<LineSegment> getSegments() {
		return segments;
	}
	
	public String toString() {
		String toReturn = "LineSegments (" + getNumSegments() + "):\n";
		for(LineSegment segment : segments) {
			toReturn += segment + "\n";
		}
		return toReturn;
	}

}

class PointList {
	HashMap<Integer, HashMap<Integer, Point>> pointsByX = new HashMap<Integer, HashMap<Integer, Point>>(); 
	HashMap<Integer, HashMap<Integer, Point>> pointsByY = new HashMap<Integer, HashMap<Integer, Point>>(); 
	
	public void addPoint(Point point) {
		Point p = get(point);
		if(p == null) p = point;
		else p = p.mergeWith(point);

		ensure(pointsByX, p.getX());
		pointsByX.get(p.getX()).put(p.getY(), p);
		
		ensure(pointsByY, p.getY());
		pointsByY.get(p.getY()).put(p.getX(), p);
	}

	public int size() {
		int num = 0;
		for(HashMap<Integer, Point> points : pointsByX.values()) {
			num += points.size();
		}
		System.out.println("pointsByX: " + num);
		num = 0;
		for(HashMap<Integer, Point> points : pointsByY.values()) {
			num += points.size();
		}
		System.out.println("pointsByY: " + num);
		return num;
	}

	public Collection<PointList> splitIntoConnectedComponents() {
		ArrayList<PointList> toReturn = new ArrayList<PointList>();
		for(HashMap<Integer, Point> points : pointsByX.values()) {
			for(Point point : points.values()) {
				if(!point.isVisited()) {
					System.out.println("NOT VISITED: " + point);
					PointList list = new PointList();
					visitRecursively(point, list);
					toReturn.add(list);
				}
			}
		}
		return toReturn;
	}

	private void visitRecursively(Point point, PointList list) {
		if(point.isVisited()) return;
		point.visit();
		list.addPoint(point);
		Point north = getPointToNorthOf(point);
		if(north != null) visitRecursively(north, list);
		Point south = getPointToSouthOf(point);
		if(south != null) visitRecursively(south, list);
		Point east = getPointToEastOf(point);
		if(east != null) visitRecursively(east, list);
		Point west = getPointToWestOf(point);
		if(west != null) visitRecursively(west, list);
	}

	private Point getPointToNorthOf(Point point) {
		if(!point.hasNorth()) return null;
		HashMap<Integer, Point> list = pointsByX.get(point.getX());
		Point toReturn = null;
		for(Point p : list.values()) {
			if(p.getY() <= point.getY()) 
				continue;
			if(toReturn == null) toReturn = p;
			if(toReturn.getY() > p.getY())  toReturn = p;
		}
		System.out.println("getPointToNorthOf(" + point + "): " + toReturn);
		return toReturn;
	}

	private Point getPointToSouthOf(Point point) {
		if(!point.hasSouth()) return null;
		HashMap<Integer, Point> list = pointsByX.get(point.getX());
		Point toReturn = null;
		for(Point p : list.values()) {
			if(p.getY() >= point.getY()) 
				continue;
			if(toReturn == null) toReturn = p;
			if(toReturn.getY() < p.getY())  toReturn = p;
		}
		System.out.println("getPointToSouthOf(" + point + "): " + toReturn);
		return toReturn;
	}

	private Point getPointToEastOf(Point point) {
		if(!point.hasEast()) return null;
		HashMap<Integer, Point> list = pointsByY.get(point.getY());
		Point toReturn = null;
		for(Point p : list.values()) {
			if(p.getX() <= point.getX()) 
				continue;
			if(toReturn == null) toReturn = p;
			if(toReturn.getX() > p.getX())  toReturn = p;
		}
		System.out.println("getPointToEastOf(" + point + "): " + toReturn);
		return toReturn;
	}

	private Point getPointToWestOf(Point point) {
		if(!point.hasWest()) return null;
		HashMap<Integer, Point> list = pointsByY.get(point.getY());
		Point toReturn = null;
		for(Point p : list.values()) {
			if(p.getX() >= point.getX()) 
				continue;
			if(toReturn == null) toReturn = p;
			if(toReturn.getX() < p.getX())  toReturn = p;
		}
		System.out.println("getPointToWestOf(" + point + "): " + toReturn);
		return toReturn;
	}

	public int getNumOddVertices() {
		int num = 0;
		for(HashMap<Integer, Point> points : pointsByX.values()) {
			for(Point point : points.values()) {
				if(point.isOdd()) num++;
			}
		}
		System.out.println("getNumOddVertices(): " + num);
		return num;
	}

	private static void ensure(HashMap<Integer, HashMap<Integer, Point>> points, int val) {
		if(points.get(val) == null) {
			points.put(val, new HashMap<Integer, Point>());
		}
	}

	public Point get(Point point) {
		return get(point.getX(), point.getY());
	}

	private Point get(int x, int y) {
		HashMap<Integer, Point> yMap = pointsByX.get(x);
		if(yMap == null) return null;
		return yMap.get(y);
	}
	
}

class Point {
	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", hasNorth=" + hasNorth
				+ ", hasSouth=" + hasSouth + ", hasEast=" + hasEast
				+ ", hasWest=" + hasWest + ", visited=" + visited + "]";
	}

	private int x, y;
	private boolean hasNorth;
	private boolean hasSouth;
	private boolean hasEast;
	private boolean hasWest;
	private boolean visited = false;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean hasNorth() {
		return hasNorth;
	}

	public boolean hasSouth() {
		return hasSouth;
	}

	public boolean hasEast() {
		return hasEast;
	}

	public boolean hasWest() {
		return hasWest;
	}

	public void visit() {
		visited = true;
	}

	public boolean isVisited() {
		return visited;
	}

	public boolean isOdd() {
		int numPaths = 0;
		if(hasNorth) numPaths++;
		if(hasSouth) numPaths++;
		if(hasEast) numPaths++;
		if(hasWest) numPaths++;
		boolean isOdd = (numPaths % 2) != 0;
		System.out.println("Point " + this + " is odd:" + isOdd);
		return isOdd;
	}

	public Point mergeWith(Point that) {
		// Assume coordinates match
		Point toReturn = new Point(x, y);
		toReturn.hasNorth = hasNorth || that.hasNorth;
		toReturn.hasSouth = hasSouth || that.hasSouth;
		toReturn.hasEast = hasEast || that.hasEast;
		toReturn.hasWest = hasWest || that.hasWest;
		System.out.println("Merging point " + this + " with " + that + ": " + toReturn);
		return toReturn;
	}

	public void setHasWest() {
		hasWest = true;
	}

	public void setHasEast() {
		hasEast = true;
	}

	public void setHasSouth() {
		hasSouth = true;
	}

	public void setHasNorth() {
		this.hasNorth = true;
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public static Point intersect(LineSegment segment1, LineSegment segment2) {

		System.out.println("Intersecting " + segment1 + " with " + segment2);
		// We only care if one is vertical and the other is horizontal
		LineSegment verticalSegment, horizontalSegment;
		
		if(segment1.isVertical() && !segment2.isVertical()) {
			verticalSegment = segment1;
			horizontalSegment = segment2;
		} else if(!segment1.isVertical() && segment2.isVertical()) {
			verticalSegment = segment2;
			horizontalSegment = segment1;					
		} else {
			return null;
		}
		

		// 5 cases -- crossing, plus each of the four points could be in line with the other line
		// plus 4 literal corner cases!
		
		// so, get the intersection point, then just check all four directions
		int intersectionX = verticalSegment.getX1();
		int intersectionY = horizontalSegment.getY1();
		
		// Do the segments not intersect?
		if(intersectionX < horizontalSegment.getX1()) return null;
		if(intersectionX > horizontalSegment.getX2()) return null;
		if(intersectionY < verticalSegment.getY1()) return null;
		if(intersectionY > verticalSegment.getY2()) return null;
		
		Point p = new Point(intersectionX, intersectionY);
		p.hasNorth = true;
		p.hasSouth = true;
		p.hasEast = true;
		p.hasWest = true;

		if(intersectionX == horizontalSegment.getX1()) p.hasWest = false;
		if(intersectionX == horizontalSegment.getX2()) p.hasEast = false;
		if(intersectionY == verticalSegment.getY1()) p.hasSouth = false;
		if(intersectionY == verticalSegment.getY2()) p.hasNorth = false;
		
		System.out.println("    Intersect: " + p);
		return p;
	}
	
}

class LineSegment {
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineSegment other = (LineSegment) obj;
		if (vertical != other.vertical)
			return false;
		if (x1 != other.x1)
			return false;
		if (x2 != other.x2)
			return false;
		if (y1 != other.y1)
			return false;
		if (y2 != other.y2)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LineSegment [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2="
				+ y2 + ", vertical=" + vertical + "]";
	}

	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private boolean vertical;

	public boolean isVertical() {
		return vertical;
	}

	public Point getPoint1() {
		return new Point(x1, y1);
	}

	public Point getPoint2() {
		return new Point(x2, y2);
	}
	
	public int getX1() {
		return x1;
	}

	public int getY1() {
		return y1;
	}

	public int getX2() {
		return x2;
	}

	public int getY2() {
		return y2;
	}

	public LineSegment(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		init();
	}
	
	public void init() {
		if(x1 == x2) vertical = true;
		
		if(x1 > x2) {
			int tmp = x1;
			x1 = x2;
			x2 = tmp;
		}
		
		if(y1 > y2) {
			int tmp = y1;
			y1 = y2;
			y2 = tmp;
		}
	}

	public LineSegment(String segment) {
		String[] parts = segment.split(" ");
		x1 = Integer.parseInt(parts[0]);
		y1 = Integer.parseInt(parts[1]);
		x2 = Integer.parseInt(parts[2]);
		y2 = Integer.parseInt(parts[3]);
		init();
	}

	public LineSegment combineWith(LineSegment ls) {
		if (!overlapsWith(ls))
			return null;
		System.out.println("Combining " + this + " with " + ls);
		int minX1 = Math.min(x1, ls.x1);
		int maxX2 = Math.max(x2, ls.x2);
		int minY1 = Math.min(y1, ls.y1);
		int maxY2 = Math.max(y2, ls.y2);
		LineSegment combined = new LineSegment(minX1, minY1, maxX2, maxY2);
		System.out.println("Returning: " + combined);
		return combined;
	}
	
	public boolean overlapsWith(LineSegment ls) {
		if (vertical != ls.vertical)
			return false;
		if (vertical) {
			if(x1 != ls.x1) return false;
			return OneDOverlaps(y1, y2, ls.y1, ls.y2);
		} else {
			if(y1 != ls.y1) return false;
			return OneDOverlaps(x1, x2, ls.x1, ls.x2);
		}
	}

	public static boolean OneDOverlaps(int x1a, int x2a, int x1b, int x2b) {
		// 5 cases:
		// a: ---  --- -----  --- ---
		// b: --- ----- ---  ---   ---
		//if(x1a == x1b && x2a == x2b) return true;
		if (x1a >= x1b && x2a <= x2b)
			return true;
		if (x1a <= x1b && x2a >= x2b)
			return true;
		if (x1a >= x1b && x1a <= x2b)
			return true;
		if (x2a >= x1b && x2a <= x2b)
			return true;
		return false;
	}

}