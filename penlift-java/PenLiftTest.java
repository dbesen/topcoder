import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PenLiftTest {

    protected PenLift solution;

    @Before
    public void setUp() {
        solution = new PenLift();
    }

    @Test(timeout = 2000)
    public void testCase0() {
        String[] segments = new String[]{"-10 0 10 0", "0 -10 0 10"};
        int n = 1;

        int expected = 1;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase1() {
        String[] segments = new String[]{"-10 0 0 0", "0 0 10 0", "0 -10 0 0", "0 0 0 10"};
        int n = 1;

        int expected = 1;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase2() {
        String[] segments = new String[]{"-10 0 0 0", "0 0 10 0", "0 -10 0 0", "0 0 0 10"};
        int n = 4;

        int expected = 0;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase3() {
        String[] segments = new String[]{"0 0 1 0", "2 0 4 0", "5 0 8 0", "9 0 13 0", "0 1 1 1", "2 1 4 1", "5 1 8 1", "9 1 13 1", "0 0 0 1", "1 0 1 1", "2 0 2 1", "3 0 3 1", "4 0 4 1", "5 0 5 1", "6 0 6 1", "7 0 7 1", "8 0 8 1", "9 0 9 1", "10 0 10 1", "11 0 11 1", "12 0 12 1", "13 0 13 1"};
        int n = 1;

        int expected = 6;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase3FirstSquare() {
        String[] segments = new String[]{"0 0 1 0", "0 1 1 1", "0 0 0 1", "1 0 1 1"};
        int n = 1;

        int expected = 0;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase4() {
        String[] segments = new String[]{"-2 6 -2 1", "2 6 2 1", "6 -2 1 -2", "6 2 1 2", "-2 5 -2 -1", "2 5 2 -1", "5 -2 -1 -2", "5 2 -1 2", "-2 1 -2 -5", "2 1 2 -5", "1 -2 -5 -2", "1 2 -5 2", "-2 -1 -2 -6", "2 -1 2 -6", "-1 -2 -6 -2", "-1 2 -6 2"};
        int n = 5;

        int expected = 3;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

    @Test(timeout = 2000)
    public void testCase5() {
        String[] segments = new String[]{"-252927 -1000000 -252927 549481", "628981 580961 -971965 580961", "159038 -171934 159038 -420875", "159038 923907 159038 418077", "1000000 1000000 -909294 1000000", "1000000 -420875 1000000 66849", "1000000 -171934 628981 -171934", "411096 66849 411096 -420875", "-1000000 -420875 -396104 -420875", "1000000 1000000 159038 1000000", "411096 66849 411096 521448", "-971965 580961 -909294 580961", "159038 66849 159038 -1000000", "-971965 1000000 725240 1000000", "-396104 -420875 -396104 -171934", "-909294 521448 628981 521448", "-909294 1000000 -909294 -1000000", "628981 1000000 -909294 1000000", "628981 418077 -396104 418077", "-971965 -420875 159038 -420875", "1000000 -1000000 -396104 -1000000", "-971965 66849 159038 66849", "-909294 418077 1000000 418077", "-909294 418077 411096 418077", "725240 521448 725240 418077", "-252927 -1000000 -1000000 -1000000", "411096 549481 -1000000 549481", "628981 -171934 628981 923907", "-1000000 66849 -1000000 521448", "-396104 66849 -396104 1000000", "628981 -1000000 628981 521448", "-971965 521448 -396104 521448", "-1000000 418077 1000000 418077", "-1000000 521448 -252927 521448", "725240 -420875 725240 -1000000", "-1000000 549481 -1000000 -420875", "159038 521448 -396104 521448", "-1000000 521448 -252927 521448", "628981 580961 628981 549481", "628981 -1000000 628981 521448", "1000000 66849 1000000 -171934", "-396104 66849 159038 66849", "1000000 66849 -396104 66849", "628981 1000000 628981 521448", "-252927 923907 -252927 580961", "1000000 549481 -971965 549481", "-909294 66849 628981 66849", "-252927 418077 628981 418077", "159038 -171934 -909294 -171934", "-252927 549481 159038 549481"};
        int n = 824759;

        int expected = 19;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
	public void testConstructLineSegment() {
		String segment = "0 1 2 3";
		LineSegment ls = new LineSegment(segment);
		assertEquals(0, ls.getX1());
		assertEquals(1, ls.getY1());
		assertEquals(2, ls.getX2());
		assertEquals(3, ls.getY2());
	}
    
    @Test
    public void testJustOverlapping() {
        String[] segments = new String[]{"0 0 0 5", "0 3 0 7"};
        int n = 1;

        int expected = 0;
        int actual = solution.numTimes(segments, n);

        Assert.assertEquals(expected, actual);
    }

	@Test
	public void testOverlaps() {
		LineSegment ls = new LineSegment("0 0 0 1");
		LineSegment ls2 = new LineSegment("0 0 0 1");
		assertTrue(ls.overlapsWith(ls));
		assertTrue(ls2.overlapsWith(ls2));
		assertTrue(ls.overlapsWith(ls2));
		assertTrue(ls2.overlapsWith(ls));
	}

	@Test
	public void testEdgeOverlapsVertical() {
		LineSegment ls = new LineSegment("0 0 0 1");
		LineSegment ls2 = new LineSegment("0 1 0 2");
		assertTrue(ls.overlapsWith(ls));
		assertTrue(ls2.overlapsWith(ls2));
		assertTrue(ls.overlapsWith(ls2));
		assertTrue(ls2.overlapsWith(ls));
	}

	@Test
	public void testNotOverlapsVertical() {
		LineSegment ls = new LineSegment("0 0 0 1");
		LineSegment ls2 = new LineSegment("0 2 0 3");
		assertTrue(ls.overlapsWith(ls));
		assertTrue(ls2.overlapsWith(ls2));
		assertFalse(ls.overlapsWith(ls2));
		assertFalse(ls2.overlapsWith(ls));
	}

	@Test
	public void testOneDOverlaps() {
		assertTrue(LineSegment.OneDOverlaps(1, 2, 1, 2));
		assertTrue(LineSegment.OneDOverlaps(1, 2, 2, 3));
		assertTrue(LineSegment.OneDOverlaps(2, 3, 1, 2));
		assertTrue(LineSegment.OneDOverlaps(1, 3, 1, 2));
		assertTrue(LineSegment.OneDOverlaps(1, 2, 1, 3));
		assertTrue(LineSegment.OneDOverlaps(0, 2, 1, 2));
		assertTrue(LineSegment.OneDOverlaps(1, 2, 0, 2));

		assertTrue(LineSegment.OneDOverlaps(1, 3, 2, 4));
		assertTrue(LineSegment.OneDOverlaps(2, 4, 1, 3));
		
		assertFalse(LineSegment.OneDOverlaps(1, 2, 3, 4));
		assertFalse(LineSegment.OneDOverlaps(3, 4, 1, 2));
	}

	@Test
	public void testCombineOverlapping() {
		String[] segments = new String[] { "0 0 0 1", "0 0 0 2" };
		LineSegments ls = new LineSegments(segments);
		ls.combineOverlapping();
		assertEquals(1, ls.getNumSegments());
		assertEquals(new LineSegment("0 0 0 2"), ls.getSegments().iterator()
				.next());
	}

	@Test
	public void testCombineOverlappingThreeOverlap() {
		String[] segments = new String[] { "0 0 0 1", "0 0 0 2", "0 0 0 3" };
		LineSegments ls = new LineSegments(segments);
		ls.combineOverlapping();
		assertEquals(1, ls.getNumSegments());
		assertContains(ls, new LineSegment("0 0 0 3"));
	}

	@Test
	public void testCombineOverlappingTwoExtendFirst() {
		String[] segments = new String[] { "0 1 0 4", "0 0 0 2", "0 3 0 5" };
		LineSegments ls = new LineSegments(segments);
		ls.combineOverlapping();
		assertEquals(1, ls.getNumSegments());
		assertContains(ls, new LineSegment("0 0 0 5"));
	}

	@Test
	public void testCombineOverlappingCross() {
		String[] segments = new String[] { "-10 0 0 0", "0 0 10 0", "0 -10 0 0", "0 0 0 10" };
		LineSegments ls = new LineSegments(segments);
		ls.combineOverlapping();
		System.out.println(ls);
		assertEquals(2, ls.getNumSegments());
		assertContains(ls, new LineSegment("-10 0 10 0"));
		assertContains(ls, new LineSegment("0 -10 0 10"));
	}

	private void assertContains(LineSegments ls, LineSegment lineSegment) {
		List<LineSegment> segments = ls.getSegments();
		assertTrue("Expected to find " + lineSegment, segments.contains(lineSegment));
	}
}
