public class PowerOutage {
	
	private int[] fromJunction;
	private int[] toJunction;
	private int[] ductLength;

	public int estimateTimeOut(int[] fromJunction, int[] toJunction, int[] ductLength) {
		this.fromJunction = fromJunction;
		this.toJunction = toJunction;
		this.ductLength = ductLength;
		int totalDistance = getTotalLength(0);
		System.out.println("Total distance is " + totalDistance);
		totalDistance -= findLongestPathLength();
		return totalDistance;
	}
	
	public int getTotalLength(int currentJunction) {
		int totalLength = 0;
		for(int i=0;i<fromJunction.length;i++) {
			if(fromJunction[i] != currentJunction) continue;
			totalLength += ductLength[i];
			totalLength += getTotalLength(toJunction[i]);
			totalLength += ductLength[i];
		}
		return totalLength;
	}
	
	public int findLongestPathLength() {
		int longestPathLength = 0;
		for(int i=0;i<fromJunction.length;i++) {
			int pathLength = getPathLength(toJunction[i], 0);
			System.out.println("path length for index " + i + " is " + pathLength);
			if(pathLength > longestPathLength) {
				longestPathLength = pathLength;
			}
		}
		System.out.println("Longest is " + longestPathLength);
		return longestPathLength;
	}

	private int getPathLength(int junction, int lengthSoFar) {
		if(junction == 0)
			return lengthSoFar;
		for(int i=0;i<toJunction.length;i++) {
			if(toJunction[i] == junction)
				return getPathLength(fromJunction[i], lengthSoFar + ductLength[i]);
		}
		return lengthSoFar;
	}

}
