import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Bonuses {

	public int[] getDivision(int[] points) {
		int[] toReturn = new int[points.length];
		int total = sum(points);
		for(int i=0;i<points.length;i++) {
			toReturn[i] = (points[i] * 100) / total;
		}
		int to_add_back = 100 - sum(toReturn);
		List<Thing> things = makeThings(points);
		Collections.sort(things);
		for(int i=0;i<to_add_back;i++) {
			int to_add_index = things.get(i).index;
			toReturn[to_add_index]++;
		}
		return toReturn;
	}

	private List<Thing> makeThings(int[] points) {
		List<Thing> things = new ArrayList<Thing>();
		for(int i=0;i<points.length;i++) {
			things.add(new Thing(points[i], i));
		}
		return things;
	}
	
	public int sum(int[] points) {
		int total = 0;
		for(int i : points)
			total += i;
		return total;
	}
	
	class Thing implements Comparable<Thing> {
		int points;
		int index;
		public Thing(int points, int index) {
			this.points = points;
			this.index = index;
		}
		public int compareTo(Thing arg0) {
			int result = arg0.points - this.points;
			if(result == 0) result = this.index - arg0.index;
			return result;
		}
		
		public String toString() {
			return "Thing[" + index + "]: " + points;
		}
	}

}
