public class Time {

	public String whatTime(int seconds) {
		int hours = seconds / 3600;
		seconds = seconds % 3600;
		int minutes = seconds / 60;
		seconds = seconds % 60;
		return "" + hours + ":" + minutes + ":" + seconds;
	}

}
