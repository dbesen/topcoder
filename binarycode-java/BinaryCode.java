public class BinaryCode {

	public String[] decode(String message) {
		String[] ret = new String[2];
		ret[0] = method(message, "0");
		ret[1] = method(message, "1");
		return ret;
	}

	private String method(String message, String decrypted) {
		for(int i=0;i<message.length();i++) {
			int haveSoFar = getVal(decrypted, i-1) + getVal(decrypted, i) + getVal(decrypted, i+1);
			int currVal = getVal(message, i) - haveSoFar;
			if(currVal > 1 || currVal < 0) return "NONE";
			if(i == message.length()-1) {
				if(currVal != 0) return "NONE";
				return decrypted;
			}
			decrypted = decrypted + currVal;
		}
		return decrypted;
	}
	
	private int getVal(String item, int position) {
		if(position < 0) return 0;
		if(position >= item.length()) return 0;
		char curr = item.charAt(position);
		return curr - '0';
	}

}
