import java.util.Random;


public class Solution {
	private double rawScore;
	private boolean scoreCalculated = false;
	static Random rand = new Random();
	
	private double data[][] = new double[92][20];
	public Solution() {
		for(int i=0;i<data.length;i++) {
			for(int j=0;j<data[i].length;j+=2) {
				// todo: what should the initial state be..?
				data[i][j] = 180;
				data[i][j+1] = 0;
			}
		}
	}
	
	public Solution(double[][] data) {
		this.data = data;
	}
	
	public double rawScore() {
		if(scoreCalculated) return rawScore;
		
		ConstraintsChecker checker = new ConstraintsChecker();
		checker.setBeta(ISSVis.beta);
		checker.setYaw(ISSVis.yaw);
		if(checker.errors.size() != 0) {
			printErrors(checker);
			return 0.0d;
		}

		for (int t = 0; t < ConstraintsChecker.NUM_STATES; t++) {
			checker.setDataForFrame(t, data[t]);
			if(checker.errors.size() != 0) {
				printErrors(checker);
				return 0.0d;
			}
	         checker.evaluateFrame(t);
		}
		checker.checkAnglesAndSpeeds();
		if(checker.errors.size() != 0) {
			printErrors(checker);
			return 0.0d;
		}
		checker.checkLongerons();
		if(checker.errors.size() != 0) {
			printErrors(checker);
			return 0.0d;
		}
		for (int t = 0; t < ConstraintsChecker.NUM_STATES; t++) {
			checker.evaluatePower(t);
		}
		checker.aggregateAndCheckPower();

		rawScore = checker.rawScore();
		System.out.println("Calculated new score: " + rawScore);
		scoreCalculated = true;
		return rawScore;
	}

	private void printErrors(ConstraintsChecker checker) {
		for(String error : checker.errors)
			System.out.println(error);
	}

	public Solution mutate(double mutationRate) {
		double[][] newData = cloneArray(data);
//		int which_minute = getRand(0, 91);
//		int which_value = getRand(0, 19);
//		
//		int numValuesToMutate = 184; // 10%
//		for(int i=0;i<numValuesToMutate ;i++) {
//			double newValue = (2 * mutationRate * rand.nextDouble()) - mutationRate;
//			newData[which_minute][which_value] = newData[which_minute][which_value] + newValue;
//		}

		for(int i=0;i<92;i++) {
			for(int j=0;j<20;j++) {
				double newValue = (2 * mutationRate * rand.nextDouble()) - mutationRate;
				newData[i][j] = range(newData[i][j] + newValue, -360d, 360d);
			}
		}
		return new Solution(newData);
	}
	
	public static int getRand(int min, int max) {
		return rand.nextInt(max - min + 1) + min;
	}
	
	public static double range(double val, double min, double max) {
		double range = max - min;
		while(val < min) val += range;
		while(val > max) val -= range;
		return val;
	}
	
	public static double[][] cloneArray(double[][] a) {
	    int length = a.length;
	    double[][] target = new double[length][a[0].length];
	    for (int i = 0; i < length; i++) {
	        System.arraycopy(a[i], 0, target[i], 0, a[i].length);
	    }
	    return target;
	}
	
	public String toString() {
		String toReturn = "";
		for(int i=0;i<data.length;i++) {
			for(int j=0;j<data[i].length;j++) {
				toReturn += data[i][j] + " ";
			}
			toReturn += "\n";
		}
		return toReturn;
	}
}
