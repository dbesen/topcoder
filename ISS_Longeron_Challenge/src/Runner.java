
public class Runner {
	static {
		// Setup that ISSVis needs
		ISSVis.beta = 75 ;
		ISSVis.alpha = 0 ;
		ISSVis.view_beta = 3700 ;
		ISSVis.view_alpha = 3700 ;
		ISSVis.rendering = false ;
		ISSVis.velocity_history = new double [92][10] ;
		ISSVis.position_history = new double [92][10] ;
		ISSVis.loadModel();
		ISSVis.initFormatters();
	}
	
	public static void main(String[] args) {
		int numMutations = 10000;
		double mutationRate = 180.0d;
		
		Solution s = new Solution();
		double baseScore = s.rawScore(); // precalculate so the timing is more accurate
		long startTime = System.nanoTime();
		System.out.println("Starting " + numMutations + " mutations, base score is " + baseScore);
		for(int i=0;i<numMutations;i++) {
			System.out.println(i + " mutations done");
			//System.out.println("Mutation rate: " + mutationRate);
			Solution s2 = s.mutate(mutationRate);
			s = bestSolution(s, s2, mutationRate);
			mutationRate *= 0.1d;
			if(mutationRate <= 0.0000001d) {
				System.out.println("Resetting mutation rate");
				mutationRate += 360d;
			}
		}
		double elapsedSeconds = (System.nanoTime() - startTime) / 1000000000d;
		System.out.println("Best solution: ");
		System.out.println(s);
		System.out.println("Best score: " + s.rawScore());
		System.out.println("Elapsed time: " + elapsedSeconds + " seconds");
		System.out.println("Time per solution: " + (elapsedSeconds / numMutations) + " seconds");
	}

	private static Solution bestSolution(Solution a, Solution b, double mutationRate) {
		double aScore = a.rawScore();
		double bScore = b.rawScore();
		//System.out.println(aScore + " vs " + bScore);
		if(aScore > bScore) return a;
		System.out.println("Found new, score is " + bScore + ", mutation rate was " + mutationRate);
		return b;
	}
}
